import {ADD_CHARACTER,REMOVE_CHARACTER} from "../constants";

export const addCharacterById = (id) => {
  const action = {
    type:ADD_CHARACTER,
    id  
  }
  //console.log("action in addCharacterById | ",action);
  return action;
}

export const removeCharacterById = (id) => {
  const action = {
    type:REMOVE_CHARACTER,
    id  
  }
  //console.log("action in removeCharacterById | ",action);
  return action;
}