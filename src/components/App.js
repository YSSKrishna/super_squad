import React, { Component } from 'react';
import './App.css';
import CharacterList from "./character_list";
import HeroList from "./hero_list";
import SquadStats from "./squad_stats";

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className = "row">
          <div className = "col-md-6 col-lg-4"><CharacterList /></div>
          <div className = "col-md-6 col-lg-4"><HeroList /></div>        
          <div className = "col-md-12 col-lg-4"><SquadStats /></div>        
        </div>
      </div>
    );
  }
}

export default App;
