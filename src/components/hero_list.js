import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {removeCharacterById} from "../actions";

class HeroList extends Component {
  
  render() {
    //console.log("this.props",this.props);
    return (
      <div>
        <h1>My Current Squad</h1>
        <ul className = "list-group">
          {this.props.heroes.map(hero => {
            return(
              <li key = {hero.id} className = "list-group-item">
                <div className = "list-item">{hero.name}</div>
                <div
                  className = "list-item right-button"
                  onClick = {()=> this.props.removeCharacterById(hero.id)}
                  >
                -</div>
              </li>
            )
          })}
        </ul>
      </div>
    );
  }
}

function mapStateToProps(state){
  //console.log("state from hero list",state);
  return {heroes : state.heroes};
}
function mapDispatchToProps(dispatch){
  return bindActionCreators({ removeCharacterById }, dispatch);
}

export default connect(mapStateToProps,mapDispatchToProps)(HeroList);
//export default connect(mapStateToProps,null)(HeroList);
