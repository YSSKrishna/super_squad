import React, { Component } from 'react';
import {connect} from 'react-redux';

class SquadStats extends Component {
  constructor(props){
    super(props);
    this.getTotalStrenth = this.getTotalStrenth.bind(this);
    this.getTotalIntelligence = this.getTotalIntelligence.bind(this);
    this.getTotalSpeed = this.getTotalSpeed.bind(this);
  }
  
  
  getTotalStrenth(heroes){
    let counter = 0;
    heroes.forEach(hero => counter += hero.strength);
    return(counter);
  }
  
  getTotalIntelligence(heroes){
    let counter = 0;
    heroes.forEach(hero => counter += hero.intelligence);
    return(counter);
  }
  
  getTotalSpeed(heroes){
    let counter = 0;
    heroes.forEach(hero => counter += hero.speed);
    return(counter);
  }
  render() {
    //console.log("this.props | squad stats ",this.props);
    return (
      <div>
        <h3>My Current Squad Stats</h3>
        <ul className = "list-group">
          <li className = "list-group-item">
            OverAll Strength : {this.getTotalStrenth(this.props.heroes)}   
          </li>
          <li className = "list-group-item">
            OverAll Intelligence : {this.getTotalIntelligence(this.props.heroes)}   
          </li>
          <li className = "list-group-item">
            OverAll Speed :  {this.getTotalSpeed(this.props.heroes)}    
          </li>
        </ul>
      </div>
    );
  }
}

function mapStateToProps(state){
  //console.log("state from hero list",state);
  return {heroes : state.heroes};
}

export default connect(mapStateToProps,null)(SquadStats);
